#!/usr/bin/env python3

import datetime
import json
import logging

# from keras.utils import plot_model
from sklearn.metrics import classification_report, confusion_matrix
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard

import constants
import dataset
from models import create_model


def run_model(version):
    model = create_model(version)

    # plot_model(model, show_shapes=True)

    # print(model.summary())
    print(model.summary())

    es = EarlyStopping()

    model.fit(x_train, y_train, epochs=8, validation_data=(x_test, y_test), callbacks=[es],
              batch_size=256)

    y_pred = model.predict(x_test, verbose=1)
    y_pred_bool = list(map(lambda y: 1 if y > 0.5 else 0, y_pred))

    return classification_report(y_test, y_pred_bool, digits=5, output_dict=True), confusion_matrix(y_test, y_pred_bool).tolist()


if __name__ == '__main__':
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S')

    results = []
    for seg in range(9, 10):
        constants.SEGMENT_SIZE = seg

        x_train, x_test, y_train, y_test = dataset.load_dataset('./data')

        for i in range(2, 3):
            try:
                for _ in range(1):
                    results.append((run_model(i), i, seg))
            except:
                raise

    output = open('./output.json', 'w')

    for a, i, s in results:
        print('=' * 20 + ' [SEG={}] Model {} '.format(s, i) + '=' * 20)
        print(a[0])
        print(a[1])

    json.dump(results, output)
    output.close()
