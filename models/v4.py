from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM, Conv1D, MaxPool1D

import constants


def create_model():
    model = Sequential()
    model.add(Conv1D(20, kernel_size=3, input_shape=(constants.SEGMENT_SIZE, 5), activation='relu'))
    model.add(MaxPool1D())
    model.add(LSTM(constants.SEGMENT_SIZE * 8, activation='relu'))
    # model.add(LSTM(40, input_shape=(constants.SEGMENT_SIZE, 4), activation='relu', return_sequences=True))
    # model.add(LSTM(40, activation='relu'))
    model.add(Dense(constants.SEGMENT_SIZE * 6, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(constants.SEGMENT_SIZE * 2, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam',
                  metrics=['accuracy'])
    return model
