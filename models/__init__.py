import importlib


def create_model(version):
    module = importlib.import_module('.v{}'.format(version), package='models')
    return module.create_model()
